# 300000_scrapers

useful links:
http://www.whitebeam.org/library/guide/TechNotes/xpathtestbed.rhtm


Script                        |         Función
-------------------------------------------------------------------------------
getbdpi_books.py              | recopila los detalles de todos los items con filtro doctype == Libro
-------------------------------------------------------------------------------
getbdpi_full_proxied.py       | NO FUNCIONA, test utilización de proxies para enmascarar origen de las peticiones
-------------------------------------------------------------------------------
getbdpi_full.py               | recopila info de la lista de todos los elementos del fondo 
-------------------------------------------------------------------------------
getbdpi.py                    | combinación de los 3 filtros, institution, matter, doctype
                              | recopila info de la lista
-------------------------------------------------------------------------------
getbdpi_single_filter.py      | recopila los detalles de todos los elementos del fondo documental 
                              | por tipo de filtro para cada valor
-------------------------------------------------------------------------------

