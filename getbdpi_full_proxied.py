# -*- coding: utf-8 -*-
import requests
from lxml import etree
from tml import sqlite
import inspect
from string import Template
import math
import sys
import sqlite3

import pprint
pp = pprint.PrettyPrinter(indent=4)
from lxml.html import fromstring
from itertools import cycle
import traceback

def get_proxies():
    url = 'https://free-proxy-list.net/'
    response = requests.get(url)
    parser = fromstring(response.text)
    proxies = set()
    for i in parser.xpath("//table[contains(@id, 'proxylisttable')]/tbody/tr")[:100]:
        print(i.xpath("//td/*"))
        print("--------------------------------------------------")
        if i.xpath('.//td[5][contains(text(),"elite")]') and i.xpath('.//td[7][contains(text(),"no")]'):
            proxy = ":".join([i.xpath('.//td[1]/text()')[0], i.xpath('.//td[2]/text()')[0]])
            # print(proxy)
            proxies.add(proxy)

    # for i in parser.xpath('//tbody/tr')[:10]:
    #     if i.xpath('.//td[7][contains(text(),"yes")]'):
    #         #Grabbing IP and corresponding PORT
    #         proxy = ":".join([i.xpath('.//td[1]/text()')[0], i.xpath('.//td[2]/text()')[0]])
    #         proxies.add(proxy)
    return proxies

def get_number_of_pages(tree):
    # url = 'http://www.iberoamericadigital.net/BDPI/Search.do?sort=&pageSize=20&institution=Biblioteca+Nacional+de+Costa+Rica&pageNumber=1'
    # # print(url)
    # rt = requests.get(url)
    # tree = etree.HTML(rt.text)
    number_of_results = get_number_of_results(tree)
    # print(number_of_results)
    number_of_pages = math.ceil(number_of_results / 20)
    # print("pages: " + str(number_of_pages))
    return number_of_pages

def get_number_of_results(tree):
    number_of_results = 0
    if len(tree.xpath("//span[contains(@id, 'numero_resultados')]/text()")) > 0:
        number_of_results = clean(tree.xpath("//span[contains(@id, 'numero_resultados')]/text()")[0]).split('de ')[1].split(' para')[0].replace(".", "")
    print("results: " + str(number_of_results))
    return int(number_of_results)

def get_table_data(tree):
    table = tree.xpath("//table[contains(@class, 'recordList')]/tbody/tr")
    content = []
    for row in table:
        title = row.xpath('td/h2[@class="titleShort"]/b/a/text()')[0]
        # print(title)
        elements = []
        data_fields = row.xpath('td/div/span[@class="datos"]/text()')
        for element in data_fields:
            elements.append(clean(element))

        datos = clean(" | ".join(elements))

        link = row.xpath('td/h2[@class="titleShort"]/b/a/@href')[0]
        # print(datos)
        content.append([title, datos, link])


    #   print(len(table))
    return content


def get_full_proxied_data():
    print("get_full_proxied")
    proxies = get_proxies()
    proxy_pool = cycle(proxies)

    url = 'https://httpbin.org/ip'
    for i in range(1,45):
        #Get a proxy from the pool
        proxy = next(proxy_pool)
        print(proxy)
        print("Request #%d"%i)
        try:
            # response = requests.get(url,proxies={"http": proxy, "https": proxy})
            response = requests.get(url,proxies={"http": "http://" + proxy, "https": "http://" + proxy}, headers={'User-Agent': 'Chrome'})
            print(response.json())
        except Exception as e:
            #Most free proxies will often get connection errors. You will have retry the entire request using another proxy to work.
            #We will just skip retries as its beyond the scope of this tutorial and we are only downloading a single url
            print("Skipping. Connnection error")
            print(e.args)

    # number_of_results = 380990
    # if number_of_results > 0:
    #     index_2 = 1
    #     while index_2 <= number_of_results:
    #         new = Template('http://www.iberoamericadigital.net/BDPI/CompleteSearch.do?pageNumber=$index')
    #         url= new.substitute(index= index_2)
    #         print(url)
    #         rt = requests.get(url)
    #         tree = etree.HTML(rt.text)
    #         sqlite.dict2sqlite('completo.sqlite', 'todas_las_fichas', [get_book_data(tree)])
    #         index_2 = index_2 + 1
    return

def clean(s):
    s = s.strip('\n')
    s = s.strip('\r')
    s = s.strip('\t')
    s = s.strip()
    return s

def clean_filter(s):
    s = s.strip()
    return s.replace("Filtrar por ","")

def column_name(s):
    s = s.strip()
    return s.replace(" ","_")

def clean_option(o):
    o = o.replace(" ", "+")
    return o

def clean_iterable(iterable):
    clean_copy = []
    for item in iterable:
        clean_copy.append(clean(item))

    return clean_copy

def clean_and_concat(iterable):
    clean_copy = (" | ").join(clean_iterable(iterable))
    return clean_copy

def collapse_spaces(s):
    while "  " in s:
        # print(len(s))
        s = s.replace("  ", " ")
    return s.strip()

# get_full_data()
print(get_proxies())
get_full_proxied_data()
