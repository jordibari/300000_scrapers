# -*- coding: utf-8 -*-
import requests
from lxml import etree
from tml import sqlite
import inspect
from string import Template
import math
import sys
import sqlite3

import pprint
pp = pprint.PrettyPrinter(indent=4)

def clean(s):
    s = s.strip('\n')
    s = s.strip('\r')
    s = s.strip('\t')
    s = s.strip()
    return s

def clean_iterable(iterable):
    clean_copy = []
    for item in iterable:
        clean_copy.append(clean(item))

    return clean_copy

def clean_and_concat(iterable):
    clean_copy = (" | ").join(clean_iterable(iterable))
    return clean_copy

# url = 'http://www.iberoamericadigital.net/BDPI/Search.do?doctype=Libro&pageSize=1&pageNumber=1'
url = 'http://www.iberoamericadigital.net/BDPI/CompleteSearch.do?doctype=Libro&pageSize=1&pageNumber=1'
rt = requests.get(url)
tree = etree.HTML(rt.text)
pp.pprint(tree)

content = {}
for li in tree.xpath('//div[contains(@id, "wrap")]/div[contains(@id, "content")]/div[contains(@id, "main")]/div[contains(@class, "ficha")]/div[contains(@class, "data")]/ul/li'):
    nombre_campo = li.xpath('span[@class="titulo"]/text()')[0]
    valores_campo = li.xpath('span[@class="datos"]/text()')
    content[nombre_campo] = clean_and_concat(valores_campo)


print(content)

# for obj in data:
#     print(obj)
#     # pp.pprint(inspect.getmembers(obj))
#     for child in obj:
#         pp.pprint(child)
