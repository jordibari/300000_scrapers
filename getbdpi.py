# -*- coding: utf-8 -*-
import requests
from lxml import etree
from tml import sqlite
import inspect
from string import Template
import math
import sys
import sqlite3

import pprint
pp = pprint.PrettyPrinter(indent=4)

def getFilters():
    url = "http://www.iberoamericadigital.net/BDPI/Search.do"
    # print(url)
    rt = requests.get(url)
    tree = etree.HTML(rt.text)
    filter_names = tree.xpath("//div[contains(@class, 'filter_head')]/span/text()")
    filter_types = []
    for name in filter_names:
        clean_name = clean_filter(name)
        # print(clean_name)
        if clean_name == "institución":
            filter_types.append("institution")

        if clean_name == "materia":
            filter_types.append("matter")

        if clean_name == "tipo de documento":
            filter_types.append("doctype")


    # print(filter_types)
    all_filters = []
    for filter_type in filter_types:
        filter_options = tree.xpath("//input[contains(@name, '" + filter_type + "')][@type='checkbox']")
        this_filter = []
        for filter_option in filter_options:
            # print(filter_type + ": " + filter_option.values()[2])
            this_filter.append(filter_option.values()[2])
        all_filters.append(this_filter)
    # print(len(all_filters))
    # print(all_filters[2][0])
    # print(inspect.getmembers(all_filters[2][0]))
    return filter_types, all_filters

def get_urls():
    filter_types, all_filters = getFilters()
    all_urls = []
    for type in filter_types:
        index = filter_types.index(type)
        type_urls = []
        for option in all_filters[index]:
            prepared_option = clean_option(option)
            new = Template('http://www.iberoamericadigital.net/BDPI/Search.do?$type=$option')
            url= new.substitute(type= type,option= prepared_option)
            type_urls.append(url)
        all_urls.append(type_urls)
            # print(url)

    return all_urls

def get_number_of_pages(tree):
    # url = 'http://www.iberoamericadigital.net/BDPI/Search.do?sort=&pageSize=20&institution=Biblioteca+Nacional+de+Costa+Rica&pageNumber=1'
    # # print(url)
    # rt = requests.get(url)
    # tree = etree.HTML(rt.text)
    number_of_results = get_number_of_results(tree)
    # print(number_of_results)
    number_of_pages = math.ceil(number_of_results / 20)
    # print("pages: " + str(number_of_pages))
    return number_of_pages

def get_number_of_results(tree):
    number_of_results = 0
    if len(tree.xpath("//span[contains(@id, 'numero_resultados')]/text()")) > 0:
        number_of_results = clean(tree.xpath("//span[contains(@id, 'numero_resultados')]/text()")[0]).split('de ')[1].split(' para')[0].replace(".", "")
    print("results: " + str(number_of_results))
    return int(number_of_results)

def get_table_data(tree):
    table = tree.xpath("//table[contains(@class, 'recordList')]/tbody/tr")
    content = []
    for row in table:
        title = row.xpath('td/h2[@class="titleShort"]/b/a/text()')[0]
        # print(title)
        elements = []
        data_fields = row.xpath('td/div/span[@class="datos"]/text()')
        for element in data_fields:
            elements.append(clean(element))

        datos = clean(" | ".join(elements))

        link = row.xpath('td/h2[@class="titleShort"]/b/a/@href')[0]
        # print(datos)
        content.append([title, datos, link])


    #   print(len(table))
    return content

def get_data_1():
    print("get_data_1")
    filter_types, all_filters = getFilters()
    aux_url = Template('&pageSize=20&pageNumber=$index')
    for institution in all_filters[0]:
        for matter in all_filters[1]:
            for doc_type in all_filters[2]:
                new = Template('http://www.iberoamericadigital.net/BDPI/Search.do?institution=$institution&matter=$matter&doctype=$doc_type')
                base_url= new.substitute(institution= clean_option(institution),matter= clean_option(matter), doc_type= clean_option(doc_type))
                rt = requests.get(base_url)
                tree = etree.HTML(rt.text)
                # print(base_url)
                if get_number_of_results(tree) > 0:
                    number_of_pages = get_number_of_pages(tree)
                    index = 1
                    while index <= number_of_pages:
                        url = base_url + aux_url.substitute(index= index)
                        print(url)
                        rt = requests.get(url)
                        tree = etree.HTML(rt.text)
                        page = []
                        for row in get_table_data(tree):
                            fila = {}
                            fila['numero'] = row[0].split(".")[0]
                            fila['nombre'] = row[0].split(".")[1]
                            fila['autor'] = row[1]
                            fila['url'] = url
                            fila['link'] = row[2]
                            fila['institucion'] = institution
                            fila['matter'] = matter
                            fila['tipo_documento'] = doc_type
                            page.append(fila)

                        if len(page)>0:
                            sqlite.dict2sqlite('bdpi.sqlite', 'fichas', page)

                        index = index + 1


    return

def get_data_2():
    print("get_data_2")
    filter_types, all_filters = getFilters()

    for type in filter_types:
        index = filter_types.index(type)
        for option in all_filters[index]:
            prepared_option = clean_option(option)
            new = Template('http://www.iberoamericadigital.net/BDPI/Search.do?$type=$option')
            base_url= new.substitute(type= type,option= prepared_option)
            print(base_url)
            rt = requests.get(base_url)
            tree = etree.HTML(rt.text)
            print(get_number_of_results(tree))
            if get_number_of_results(tree) > 0:
                number_of_pages = get_number_of_pages(tree)
                index_2 = 1
                while index_2 <= number_of_pages:
                    aux_url = Template('&pageSize=20&pageNumber=$index')
                    url = base_url + aux_url.substitute(index= index_2)
                    print(url)
                    rt = requests.get(url)
                    tree = etree.HTML(rt.text)
                    page = []
                    for row in get_table_data(tree):
                        fila = {}
                        fila['numero'] = row[0].split(".")[0]
                        fila['nombre'] = row[0].split(".")[1]
                        fila['autor'] = row[1]
                        fila['url'] = url
                        fila['link'] = row[2]
                        fila[type] = option
                        page.append(fila)

                    if len(page)>0:
                        sqlite.dict2sqlite('bdpi.sqlite', 'fichas_2', page)

                    index_2 = index_2 + 1

    return

def get_book_data(tree):
    content = {}
    titulo = tree.xpath('//div[contains(@id, "wrap")]/div[contains(@id, "content")]/div[contains(@id, "main")]/div[contains(@class, "ficha")]/div[contains(@class, "data")]/div/h1[@class="titulo_detail"]/text()')
    # print('titulo1' + collapse_spaces(clean_and_concat(titulo)))
    # print('titulo2' + clean_and_concat(titulo))
    # print('titulo3' + str(len(titulo)))
    # for line in titulo:
        # print("********************************puro*")
        # print(line)
        # print("*******************************tratao**")
        # print(collapse_spaces(collapse_spaces(line)))
        # print("*********************************")
    content['titulo'] = collapse_spaces(clean_and_concat(titulo))

    autoria = tree.xpath('//div[contains(@id, "wrap")]/div[contains(@id, "content")]/div[contains(@id, "main")]/div[contains(@class, "ficha")]/div[contains(@class, "data")]/div/text()')
    # print('autoria' + collapse_spaces(clean_and_concat(autoria)))
    content['autoria'] = collapse_spaces(clean_and_concat(autoria))

    link = tree.xpath('//div[contains(@id, "wrap")]/div[contains(@id, "content")]/div[contains(@id, "main")]/div[contains(@class, "ficha")]/div[contains(@class, "data")]/div/a/@href')
    # print('linkz' + collapse_spaces(clean_and_concat(link)))
    content['link'] = collapse_spaces(clean_and_concat(link))

    for li in tree.xpath('//div[contains(@id, "wrap")]/div[contains(@id, "content")]/div[contains(@id, "main")]/div[contains(@class, "ficha")]/div[contains(@class, "data")]/ul/li'):
        if len(li.xpath('span[@class="titulo"]/text()')) > 0:
            # print(len(li.xpath('span[@class="titulo"]/text()')))
            nombre_campo = clean_and_concat(li.xpath('span[@class="titulo"]/text()'))
            # print(nombre_campo)
            valores_campo = clean_and_concat(li.xpath('span[@class="datos"]/text()'))
        else:
            nombre_campo = 'unnamed_column'
            valores_campo = clean_and_concat(li.xpath('span[@class="urllink"]/a/@href'))
        content[column_name(nombre_campo)] = valores_campo
    return content

def get_books():
    print("get_books")
    new = Template('http://www.iberoamericadigital.net/BDPI/Search.do?$type=$option')
    base_url= new.substitute(type= "doctype",option= "Libro")
    print(base_url)
    rt = requests.get(base_url)
    tree = etree.HTML(rt.text)
    number_of_results = get_number_of_results(tree)
    if number_of_results > 0:
        index_2 = 1
        while index_2 <= number_of_results:
            new = Template('http://www.iberoamericadigital.net/BDPI/CompleteSearch.do?$type=$option')
            base_url= new.substitute(type= "doctype",option= "Libro")
            aux_url = Template('&pageSize=1&pageNumber=$index')
            url = base_url + aux_url.substitute(index= index_2)
            print(url)
            rt = requests.get(url)
            tree = etree.HTML(rt.text)
            sqlite.dict2sqlite('libros.sqlite', 'libros', [get_book_data(tree)])
            index_2 = index_2 + 1
    return

def clean(s):
    s = s.strip('\n')
    s = s.strip('\r')
    s = s.strip('\t')
    s = s.strip()
    return s

def clean_filter(s):
    s = s.strip()
    return s.replace("Filtrar por ","")

def column_name(s):
    s = s.strip()
    return s.replace(" ","_")

def clean_option(o):
    o = o.replace(" ", "+")
    return o

def clean_iterable(iterable):
    clean_copy = []
    for item in iterable:
        clean_copy.append(clean(item))

    return clean_copy

def clean_and_concat(iterable):
    clean_copy = (" | ").join(clean_iterable(iterable))
    return clean_copy

def collapse_spaces(s):
    while "  " in s:
        # print(len(s))
        s = s.replace("  ", " ")
    return s.strip()

get_books()
