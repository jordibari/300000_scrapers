# -*- coding: utf-8 -*-
import requests
from lxml import etree
from tml import sqlite
from string import Template
import math
import sys
import sqlite3
from unidecode import unidecode
import os
import re

import pprint
pp = pprint.PrettyPrinter(indent=4)

def getFilters():
    url = "http://www.iberoamericadigital.net/BDPI/Search.do"
    # print(url)
    rt = requests.get(url, timeout=10)
    tree = etree.HTML(rt.text)
    filter_names = tree.xpath("//div[contains(@class, 'filter_head')]/span/text()")
    filter_types = []
    for name in filter_names:
        clean_name = clean_filter(name)
        # print(clean_name)
        if clean_name == "institución":
            filter_types.append("institution")

        if clean_name == "materia":
            filter_types.append("matter")

        if clean_name == "tipo de documento":
            filter_types.append("doctype")


    # print(filter_types)
    all_filters = []
    for filter_type in filter_types:
        filter_options = tree.xpath("//input[contains(@name, '" + filter_type + "')][@type='checkbox']")
        this_filter = []
        for filter_option in filter_options:
            # print(filter_type + ": " + filter_option.values()[2])
            this_filter.append(filter_option.values()[2])
        all_filters.append(this_filter)
    # print(len(all_filters))
    # print(all_filters[2][0])
    # print(inspect.getmembers(all_filters[2][0]))
    return filter_types, all_filters

def get_urls():
    filter_types, all_filters = getFilters()
    all_urls = []
    for type in filter_types:
        index = filter_types.index(type)
        type_urls = []
        for option in all_filters[index]:
            prepared_option = clean_option(option)
            new = Template('http://www.iberoamericadigital.net/BDPI/Search.do?$type=$option')
            url= new.substitute(type= type,option= prepared_option)
            type_urls.append(url)
        all_urls.append(type_urls)
            # print(url)

    return all_urls

def get_number_of_pages(tree):
    number_of_results = get_number_of_results(tree)
    number_of_pages = math.ceil(number_of_results / 20)
    return number_of_pages

def get_number_of_results(tree):
    number_of_results = 0
    if len(tree.xpath("//span[contains(@id, 'numero_resultados')]/text()")) > 0:
        number_of_results = clean(tree.xpath("//span[contains(@id, 'numero_resultados')]/text()")[0]).split('de ')[1].split(' para')[0].replace(".", "")
    print("results: " + str(number_of_results))
    return int(number_of_results)

def get_table_data(tree):
    table = tree.xpath("//table[contains(@class, 'recordList')]/tbody/tr")
    content = []
    for row in table:
        title = row.xpath('td/h2[@class="titleShort"]/b/a/text()')[0]
        # print(title)
        elements = []
        data_fields = row.xpath('td/div/span[@class="datos"]/text()')
        for element in data_fields:
            elements.append(clean(element))

        datos = clean(" | ".join(elements))

        link = row.xpath('td/h2[@class="titleShort"]/b/a/@href')[0]
        # print(datos)
        content.append([title, datos, link])


    #   print(len(table))
    return content


def get_book_data(tree):
    content = {}
    titulo = tree.xpath('//div[contains(@id, "wrap")]/div[contains(@id, "content")]/div[contains(@id, "main")]/div[contains(@class, "ficha")]/div[contains(@class, "data")]/div/h1[@class="titulo_detail"]/text()')
    content['titulo'] = collapse_spaces(clean_and_concat(titulo))

    autoria = tree.xpath('//div[contains(@id, "wrap")]/div[contains(@id, "content")]/div[contains(@id, "main")]/div[contains(@class, "ficha")]/div[contains(@class, "data")]/div/text()')
    content['autoria'] = collapse_spaces(clean_and_concat(autoria))

    c = clean_iterable(autoria)
    c = list(filter(lambda a: a != '', c))
    if len(c)>0:
        f = re.findall(r'(\d{4})', c[-1])
        if len(f)>0:
            content['fecha1'] = f[0]
            content['fecha2'] = f[-1]

    link = tree.xpath('//div[contains(@id, "wrap")]/div[contains(@id, "content")]/div[contains(@id, "main")]/div[contains(@class, "ficha")]/div[contains(@class, "data")]/div/a/@href')
    content['link'] = collapse_spaces(clean_and_concat(link))

    for li in tree.xpath('//div[contains(@id, "wrap")]/div[contains(@id, "content")]/div[contains(@id, "main")]/div[contains(@class, "ficha")]/div[contains(@class, "data")]/ul/li'):
        if len(li.xpath('span[@class="titulo"]/text()')) > 0:
            # print(len(li.xpath('span[@class="titulo"]/text()')))
            nombre_campo = clean_and_concat(li.xpath('span[@class="titulo"]/text()'))
            # print(nombre_campo)
            valores_campo = clean_and_concat(li.xpath('span[@class="datos"]/text()'))
        else:
            nombre_campo = 'unnamed_column'
            valores_campo = clean_and_concat(li.xpath('span[@class="urllink"]/a/@href'))
        content[column_name(nombre_campo)] = valores_campo

    # print(content)
    return content

def get_full_data():
    '''
    nueva url

    http://www.iberoamericadigital.net/BDPI/CompleteSearch.do?pageSize=1&pageNumber=656000

    tambien esta:

    http://www.iberoamericadigital.net/BDPI/CompleteSearch.do?lengua=&text=&field2Op=AND&field1val=&numfields=3&pageSize=1&field3Op=AND&completeText=off&field3=todos&field3val=&field2=todos&field1Op=AND&exact=on&advanced=true&field1=todos&field2val=&pageNumber=656477

    '''
    print("get_full")

    # new = Template('http://www.iberoamericadigital.net/BDPI/Search.do?$type=$option')
    # base_url= new.substitute(type= "doctype",option= "Libro")
    # print(base_url)
    # rt = requests.get(base_url)
    # tree = etree.HTML(rt.text)

    #number_of_results = 380990
    number_of_results = 656478

    i = logread()
    if number_of_results > 0:
        index_2 = i+1
        while index_2 <= number_of_results:
            ## new = Template('http://www.iberoamericadigital.net/BDPI/CompleteSearch.do?pageNumber=$index')
            try:
                new = Template('http://www.iberoamericadigital.net/BDPI/CompleteSearch.do?pageSize=1&pageNumber=$index')
                url= new.substitute(index= index_2)
                print(url)
                rt = requests.get(url, timeout=10)
                tree = etree.HTML(rt.text)
                data = get_book_data(tree)
                data['url_bdpi'] = url
                sqlite.dict2sqlite('completo.sqlite', 'todas_las_fichas', [data])
                logwrite(index_2)
                index_2 = index_2 + 1
            except Exception as e:
                print(e)
    return


LOG = "log.txt"
def logread():
    n = 0
    if os.path.exists(LOG):
        with open(LOG, 'r') as f:
            n = f.read()
    return int(n)

def logwrite(n):
    with open(LOG, 'w') as f:
        f.write('%s' %(n))
    return

def logclean():
    os.remove(LOG)
    return


def clean(s):
    s = s.strip('\n')
    s = s.strip('\r')
    s = s.strip('\t')
    s = s.strip()
    return s

def clean_filter(s):
    s = s.strip()
    return s.replace("Filtrar por ","")

def column_name(s):
    s = s.strip()
    s = s.replace(" ","_")
    s = unidecode(s)
    s = s.lower()
    s = s.replace('_de_', '_')
    return s

def clean_option(o):
    o = o.replace(" ", "+")
    return o

def clean_iterable(iterable):
    clean_copy = []
    for item in iterable:
        clean_copy.append(clean(item))

    return clean_copy

def clean_and_concat(iterable):
    c = clean_iterable(iterable)
    c = list(filter(lambda a: a != '', c))
    clean_copy = (" | ").join(c)
    return clean_copy

def collapse_spaces(s):
    while "  " in s:
        # print(len(s))
        s = s.replace("  ", " ")
    # while '\t' in s:
    #     s = s.replace("\t", "")
    # while '\r' in s:
    #     s = s.replace("\r", "")
    # while '\n' in s:
    #     s = s.replace("\n", "")
    return s.strip()


#############################################

#logclean()
get_full_data()
